package nappakun;

import java.util.Random;

public class NotRandom extends Random {
	  private int fixed = 0;

	  public NotRandom(int fixed) {
	    this.fixed = fixed;
	  }

	  public int nextInt(int n) {
	    return this.fixed;
	  }
}
