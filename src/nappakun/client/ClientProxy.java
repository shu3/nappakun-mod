package nappakun.client;

import nappakun.CommonProxy;
import nappakun.Sounds;
import net.minecraftforge.common.MinecraftForge;

public class ClientProxy extends CommonProxy {
	  public void registerRenderers()
	  {
		  MinecraftForge.EVENT_BUS.register(new Sounds());
	  }
}
