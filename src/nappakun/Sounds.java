package nappakun;

import net.minecraftforge.client.event.sound.SoundLoadEvent;
import net.minecraftforge.event.ForgeSubscribe;

public class Sounds {
	
    @ForgeSubscribe
    public void onSound(SoundLoadEvent event)
    {
        try
        {
        	//System.out.println("onSound!");
            event.manager.addSound(ModNappaKun.ModId + ":syupon.ogg");
        }
        catch (Exception e)
        {
            System.err.println("NappaKun Mod: Failed to register sounds.");
        }
    }

}
