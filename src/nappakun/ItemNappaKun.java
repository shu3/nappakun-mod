package nappakun;

import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.BlockCrops;
import net.minecraft.block.BlockSapling;
import net.minecraft.block.BlockStem;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityEgg;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;
import net.minecraft.world.World;

public class ItemNappaKun extends Item {
	  protected Icon itemCloseIcon;
	  protected Icon itemOpenIcon;
	  protected Icon itemKunFXIcon;
	  protected EntityKun thrown = null;
	  
	  public ItemNappaKun(int par1)
	  {
	    super(par1);
        this.maxStackSize = 1;
        this.setMaxDamage(100);
        this.setCreativeTab(CreativeTabs.tabTools);
	    this.setUnlocalizedName("NappaKun");
	  }
	  
	  @SideOnly(Side.CLIENT)
	  public void registerIcons(IconRegister par1IconRegister) {
		  this.itemCloseIcon = par1IconRegister.registerIcon("NappaKun:kunclose");
		  this.itemOpenIcon = par1IconRegister.registerIcon("NappaKun:kunopen");
		  this.itemKunFXIcon = par1IconRegister.registerIcon("NappaKun:kunfx");
		  this.itemIcon = this.itemCloseIcon;
	  }
	  
	  public boolean onItemUse(ItemStack par1ItemStack, EntityPlayer player, World world, int x, int y, int z, int par7, float par8, float par9, float par10)
	  {
		    int blockid = world.getBlockId(x, y, z);
		    Block block = Block.blocksList[blockid];
		    if (block != null) {
		      if (Block.plantYellow.canBlockStay(world, x, y + 1, z)) {
		        world.setBlock(x, y + 1, z, Block.plantYellow.blockID);
		        world.playSoundAtEntity(player, "random.pop", 2.0F, 1.2F);
		        return true;
		      }
		      if (((block instanceof BlockSapling)) || ((block instanceof BlockCrops)) || ((block instanceof BlockStem)))
		      {
		        world.playSoundAtEntity(player, "step.cloth", 2.0F, 1.2F);
		        block.updateTick(world, x, y, z, new NotRandom(0));
		        return true;
		      }
		    }

		    return false;
	  }
	    
	  public ItemStack onItemRightClick(ItemStack par1ItemStack, World world, EntityPlayer player)
	  {
		  if (!world.isRemote)
		  {
			  // 連打しすぎるとNullPointerがたまに発生するため一定間隔間をあける
			  if (thrown != null && thrown.countToDead < 90 && !thrown.isDead) {
				  return par1ItemStack;
			  }
			  
			  EntityKun kun = new EntityKun(world, player);
			  kun.setThrowableHeading(kun.motionX, kun.motionY, kun.motionZ, 2.0F, 1.0F);
			  world.spawnEntityInWorld(kun);
			  thrown = kun;
			  
			  EntityKunFx fx = new EntityKunFx(
					  world, kun.posX, kun.posY, kun.posZ,
					  kun.motionX, kun.motionY, kun.motionZ, this.itemKunFXIcon);
			  //System.out.println("addEffect: " + fx);
			  FMLClientHandler.instance().getClient().effectRenderer.addEffect(fx);
		  }

		  par1ItemStack.damageItem(1, player);
		  this.itemIcon = itemOpenIcon;
		  player.setItemInUse(par1ItemStack, this.getMaxItemUseDuration(par1ItemStack));

		  return par1ItemStack;
	  }
	    
	  @Override
	  public void onPlayerStoppedUsing(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer, int par4)
	  {
		  this.itemIcon = itemCloseIcon;
	  }

	  /**
	   * How long it takes to use or consume an item
	   */
	  public int getMaxItemUseDuration(ItemStack par1ItemStack)
	  {
		  return 72000;
	  }

}
