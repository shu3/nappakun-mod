package nappakun;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityAgeable;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.passive.EntityChicken;
import net.minecraft.entity.passive.EntityCow;
import net.minecraft.entity.passive.EntityPig;
import net.minecraft.entity.passive.EntitySheep;
import net.minecraft.entity.passive.EntityVillager;
import net.minecraft.entity.projectile.EntityEgg;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.util.EnumMovingObjectType;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

public class EntityKun extends EntityThrowable {
	private EntityLivingBase player;
	public int countToDead = 100;

    public EntityKun(World par1World)
    {
        super(par1World);
    }
    
	public EntityKun(World par1World, EntityLivingBase player) {
		super(par1World, player);
		setSize(0.1F, 0.1F);
		this.player = player;
	}

    public EntityKun(World par1World, double par2, double par4, double par6)
    {
        super(par1World, par2, par4, par6);
    }

	protected void onImpact(MovingObjectPosition mpos) {
	    int interval = (int)(Math.abs(this.player.posX - this.posX) + Math.abs(this.player.posY - this.posY) + Math.abs(this.player.posZ - this.posZ));
	    if (interval > 10) {
	    	this.worldObj.createExplosion(this, this.posX, this.posY, this.posZ, 10.0F, true);
	    }
	    else {
	    	if (mpos.typeOfHit == EnumMovingObjectType.ENTITY) {
	    		Entity e = mpos.entityHit;
	    		EntityAgeable child = null;
	    		double x = player.posX; double y = player.posY; double z = player.posZ;
	    		
	    		if (e instanceof EntityChicken) {
	    			e.playSound("mob.chicken.hurt", 2.0F, 1.0F);
	    			e.playSound("mob.chicken.plop", 2.0F, 1.0F);
	    			child = ((EntityChicken) e).createChild(null);
	    		}
	    		else if (e instanceof EntityCow) {
	    			e.playSound("mob.cow.hurt", 2.0F, 1.0F);
	    			e.playSound("mob.chicken.plop", 2.0F, 1.0F);
	    			child = ((EntityCow) e).createChild(null);
	    		}
	    		else if (e instanceof EntityPig) {
	    			e.playSound("mob.pig.say",2.0F, 1.0F);
	    			e.playSound("mob.chicken.plop", 2.0F, 1.0F);
	    			child = ((EntityPig) e).createChild(null);
	    		}
	    		else if (e instanceof EntitySheep) {
	    			e.playSound("mob.sheep.say", 2.0F, 1.0F);
	    			((EntitySheep) e).setSheared(false);
	    		}
	    		else if (e instanceof EntityVillager) {
	    			e.playSound("mob.villager.haggle", 2.0F, 1.0F);
	    	        ((EntityVillager) e).setHealth(0.0F);
	    		}
	    		else if (e instanceof EntityLiving) {
	    			e.playSound(ModNappaKun.ModId + ":syupon", 2.0F, 1.0F);
	    			e.motionY += 3.0D;
	    		}

	    		if (child != null) {
	    			child.setGrowingAge(-24000);
	    			child.setLocationAndAngles(e.posX, e.posY, e.posZ, 0.0F, 0.0F);
	    			worldObj.spawnEntityInWorld(child);
	    		}
	    	}
	    }
	    
	    if (!worldObj.isRemote) {
	    	setDead();
	    }
	}

	public void onUpdate() {
		super.onUpdate();
		
        for (int j = 0; j < 8; ++j)
        {
        	//System.out.println("x = " + this.posX + ", y = " + this.posY + ", z = " + this.posZ);
            this.worldObj.spawnParticle("snowballpoof", this.posX, this.posY, this.posZ, 0.0D, 0.0D, 0.0D);
        }
		
		this.countToDead -= 1;
	    if (!worldObj.isRemote) {
	        if (this.countToDead < 0)
	            setDead();
	    }
	}
	
	@Override
	protected float getGravityVelocity() {
		return 0.0F;
	}
}
