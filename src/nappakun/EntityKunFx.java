package nappakun;

import net.minecraft.block.Block;
import net.minecraft.client.particle.EntityFX;
import net.minecraft.util.Icon;
import net.minecraft.world.World;

public class EntityKunFx extends EntityFX {
	  public EntityKunFx(World par1World, double par2, double par4, double par6, double par8, double par10, double par12)
	  {
	    super(par1World, par2, par4, par6, par8, par10, par12);
	  }
	  
	  public EntityKunFx(World par1World, double par2, double par4, double par6, double par8, double par10, double par12, Icon icon)
	  {
	    super(par1World, par2, par4, par6, par8, par10, par12);
	    setParticleIcon(icon);
        this.particleRed = this.particleGreen = this.particleBlue = 1.0F;
        this.particleScale /= 1.0F;
	  }
	  
	  public int getFXLayer() {
		  return 2;
	  }
}
