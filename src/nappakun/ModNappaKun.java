package nappakun;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
import java.io.PrintStream;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.Configuration;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.Property;

@Mod(modid="NappaKun", name="NappaKun", version="0.0.1")
@NetworkMod(clientSideRequired=true, serverSideRequired=false, versionBounds="1.6.4")
public class ModNappaKun {
	  @Mod.Instance("NappaKun")
	  public static ModNappaKun instance;

	  @SidedProxy(clientSide="nappakun.client.ClientProxy", serverSide="nappakun.CommonProxy")
	  public static CommonProxy proxy;
	  public static final String ModId = "NappaKun";
	  public static int kunID;
	  public static Item kunItem;

	  @Mod.EventHandler
	  public void preInit(FMLPreInitializationEvent event)
	  {
		  try
		  {
			  Configuration config = new Configuration(event.getSuggestedConfigurationFile());
			  config.load();
			  kunID = config.getItem("NappaKun", 717, "ID for Kun-item").getInt();

			  config.save();
		  } catch (ExceptionInInitializerError e) {
			  System.out.println(e.getCause().getMessage());
			  e.getCause().printStackTrace();
			  throw e;
		  }
	  }
	  

	  @Mod.EventHandler
	  public void load(FMLInitializationEvent event)
	  {
		  kunItem = new ItemNappaKun(kunID);

		  GameRegistry.addRecipe(new ItemStack(kunItem, 1),
				  new Object[] { "L  ", "LLL", "LLL",
			  Character.valueOf('L'), Block.waterlily});

		  LanguageRegistry.addName(kunItem, "NappaKun");
		  LanguageRegistry.instance().addNameForObject(kunItem, "ja_JP", "クンッ");

		  proxy.registerRenderers();
	  }

	  @Mod.EventHandler
	  public void postInit(FMLPostInitializationEvent event)
	  {
	  }
	    
}
